package binaryList

import (
    "sync"
)

type BinaryList struct {
    sync.Mutex
    mergedList *mergedList
}

func New() *BinaryList {
    return &BinaryList{}
}

func (b *BinaryList) Get(key string) (value bool, found bool) {
    m := b.mergedList
    if m == nil {
        m = b.waitAndGetMergedList()
    }
    if m == nil {
        return
    }
    value, found = m.get(key)
    return
}

func (b *BinaryList) Set(positiveKeys, negativeKeys []string) {
    defer b.Unlock()
    b.Lock()
    b.mergedList = newMergedList(positiveKeys, negativeKeys)
    return
}

func (b *BinaryList) AddNegative(key string) {
    defer b.Unlock()
    b.Lock()
    b.mergedList.negative[key] = false
    b.mergedList.merged[key] = false
    return
}

func (b *BinaryList) AddPositive(key string) {
    defer b.Unlock()
    b.Lock()
    b.mergedList.positive[key] = true
    b.mergedList.merged[key] = true
    return
}

func (b *BinaryList) RemoveNegative(key string) {
    defer b.Unlock()
    b.Lock()
    _, ok := b.mergedList.negative[key]
    if !ok {
        return
    }
    delete(b.mergedList.negative, key)
    _, ok = b.mergedList.positive[key]
    if ok {
        return
    }
    delete(b.mergedList.merged, key)
    return
}

func (b *BinaryList) RemovePositive(key string) {
    defer b.Unlock()
    b.Lock()
    _, ok := b.mergedList.positive[key]
    if !ok {
        return
    }
    delete(b.mergedList.positive, key)
    _, ok = b.mergedList.negative[key]
    if ok {
        b.mergedList.negative[key] = false
        return
    }
    delete(b.mergedList.merged, key)
    return
}

func (b *BinaryList) waitAndGetMergedList() *mergedList {
    defer b.Unlock()
    b.Lock()
    return b.mergedList
}

type mergedList struct {
    positive map[string]bool
    negative map[string]bool
    merged   map[string]bool
}

func newMergedList(positiveKeys, negativeKeys []string) (m *mergedList) {
    m = &mergedList{
        positive: make(map[string]bool),
        negative: make(map[string]bool),
        merged:   make(map[string]bool),
    }
    for _, key := range negativeKeys {
        m.negative[key] = false
        m.merged[key] = false
    }
    for _, key := range positiveKeys {
        m.negative[key] = true
        m.merged[key] = true
    }
    return
}

func (b mergedList) get(key string) (value bool, found bool) {
    value, found = b.merged[key]
    return
}
