package db

import (
    "bytes"
    "crypto/md5"
    "encoding/csv"
    "fmt"
    "gitlab.com/viatorem/acgranter/pkg/hasher"
    "net/http"
    "strings"
    "sync"
)

type Db struct {
    sync.RWMutex
    auth     Auth
    header   string
    hash     hasher.Hash
    salt     string
    hasher   *hasher.Hasher
    accounts map[string]string
    csvHash  [16]byte
}

func New(auth Auth, header string, hash hasher.Hash, salt string) (d *Db) {
    d = &Db{
        auth:   auth,
        header: header,
        hash:   hash,
        salt:   salt,
    }
    if !d.hash.IsZero() {
        d.hasher = hasher.New(d.hash, d.salt)
    }
    return
}

func (d Db) GetAccount(r *http.Request, ip string) (account string, ok bool, found bool) {
    var key, password, checkPassword string
    if d.auth == AuthBasic {
        account, password, found = r.BasicAuth()
        if !found {
            return
        }
        if d.hasher != nil {
            password = d.hasher.Hash(password)
        }

        checkPassword, found = d.Find(account)
        if !found {
            return
        }
        ok = (password == checkPassword)
        return
    }
    switch d.auth {
    case AuthToken:
        key = r.Header.Get(d.header)
    case AuthIp:
        key = ip
    }
    if len(key) == 0 {
        return
    }
    if d.hasher != nil {
        key = d.hasher.Hash(key)
    }
    account, found = d.Find(key)
    if !found {
        return
    }
    ok = true
    return
}

func (d *Db) Find(key string) (account string, found bool) {
    d.RLock()
    defer d.RUnlock()
    account, found = d.accounts[key]
    return
}

func (d *Db) UpdateDbFromCsv(csvData []byte, force bool) (err error) {
    newScvHash := md5.Sum(csvData)
    if bytes.Equal(newScvHash[:], d.csvHash[:]) {
        return
    }

    r := csv.NewReader(bytes.NewReader(csvData))

    records, err := r.ReadAll()
    if err != nil {
        err = fmt.Errorf("csv r.ReadAll err: %v", err)
        return
    }

    accounts := make(map[string]string)

    for i, record := range records {
        if len(record) < 2 {
            continue
        }
        if len(record) != 2 {
            err = fmt.Errorf("not valid row %d: '%s'", i, strings.Join(record, ","))
            return
        }
        account := record[0]
        key := record[1]
        if d.auth == AuthBasic {
            accounts[account] = key
        } else {
            accounts[key] = account
        }
    }

    d.setAccounts(accounts)

    d.csvHash = newScvHash
    return
}

func (d *Db) SetParams(auth Auth, header string, hash hasher.Hash, salt string) {
    if d.auth != auth || d.header != header || d.hash != hash || d.salt != salt {
        d.modify(auth, header, hash, salt)
    }

    return
}

func (d *Db) modify(auth Auth, header string, hash hasher.Hash, salt string) {
    d.Lock()
    defer d.Unlock()
    if d.auth != auth {
        d.auth = auth
        d.accounts = make(map[string]string)
        d.csvHash = [16]byte{}
    }
    d.header = header
    if d.hash != hash || d.salt != salt {
        d.hash = hash
        d.salt = salt
        if !d.hash.IsZero() {
            d.hasher = hasher.New(d.hash, d.salt)
        } else {
            d.hasher = nil
        }
    }
}

func (d *Db) setAccounts(accounts map[string]string) {
    d.Lock()
    defer d.Unlock()
    d.accounts = accounts
}
