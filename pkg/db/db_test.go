package db

import (
    "github.com/stretchr/testify/assert"
    "testing"

    "gopkg.in/yaml.v2"
)

func TestAuthMarshalYAML(t *testing.T) {
    _, err := yaml.Marshal(Auth(^uint(0)))
    assert.NotNil(t, err, "Non-supported Auth must not be marshaled")

    label, err := yaml.Marshal(AuthToken)
    assert.Nil(t, err)
    assert.Equal(t, AuthTokenLabel+"\n", string(label))
}

func TestAuthUnmarshalYAML(t *testing.T) {
    var x struct {
        Value Auth `yaml:"value"`
    }
    assert.NotNil(t, yaml.Unmarshal([]byte("value: \"\""), &x), "Empty string must not be unmarshaled")

    assert.NotNil(t, yaml.Unmarshal([]byte("md4"), &x.Value), "Non-Supported hash must not be unmarshaled")

    assert.Nil(t, yaml.Unmarshal([]byte(AuthTokenLabel), &x.Value))
    assert.Equal(t, AuthToken, x.Value)
}
