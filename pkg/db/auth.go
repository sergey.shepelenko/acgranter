package db

import (
    "fmt"
)

const (
    AuthBasic Auth = iota
    AuthToken
    AuthIp
)

const (
    AuthBasicLabel = "basic"
    AuthTokenLabel = "token"
    AuthIpLabel    = "ip"
)

type Auth uint

func (a Auth) String() string {
    switch a {
    case AuthBasic:
        return AuthBasicLabel
    case AuthToken:
        return AuthTokenLabel
    case AuthIp:
        return AuthIpLabel
    }
    return ""
}

func (a *Auth) UnmarshalYAML(unmarshal func(interface{}) error) (err error) {
    var s string
    err = unmarshal(&s)
    if err != nil {
        return
    }

    switch s {
    case AuthBasicLabel:
        *a = AuthBasic
    case AuthTokenLabel:
        *a = AuthToken
    case AuthIpLabel:
        *a = AuthIp
    default:
        err = fmt.Errorf("not valid value for auth: %s", s)
        return
    }
    return nil
}

func (a Auth) MarshalYAML() (value interface{}, err error) {
    s := a.String()
    if len(s) == 0 {
        err = fmt.Errorf("Auth must not have empty label")
        return
    }
    value = s
    return
}
