package counter

import (
    "log"
    "sync"
    "time"
)

type Item struct {
    Ip         string
    Amount     int64
    LastAccess int64
}

type Counter struct {
    sync.Mutex
    m map[string]*Item
}

var (
    CheckPeriod = time.Second
)

func New(maxTTL int64) (i *Counter) {
    i = &Counter{m: make(map[string]*Item)}
    if maxTTL == 0 {
        return
    }
    go func(i *Counter) {
        for now := range time.Tick(CheckPeriod) {
            i.Lock()
            for key, item := range i.m {
                if now.Unix()-item.LastAccess > maxTTL {
                    delete(i.m, key)
                }
            }
            i.Unlock()
        }
    }(i)
    return
}

func (i *Counter) Len() int {
    defer i.Unlock()
    i.Lock()
    return len(i.m)
}

func (i *Counter) Hit(ip string, add bool) (amount int64, lastAccess time.Time) {
    defer i.Unlock()
    i.Lock()
    log.Println(ip)
    item, ok := i.m[ip]
    if !ok {
        if !add {
            return
        }
        item = &Item{
            Amount: 1,
            Ip:     ip,
        }
        i.m[ip] = item
    }
    lastAccess = time.Unix(item.LastAccess, 0)
    item.LastAccess = time.Now().Unix()
    amount = item.Amount
    item.Amount++
    return
}

func (i *Counter) Delete(ip string) {
    defer i.Unlock()
    i.Lock()
    delete(i.m, ip)

    return
}
