package server

import (
    "fmt"
    "gitlab.com/viatorem/acgranter/pkg/db"
    "gitlab.com/viatorem/acgranter/pkg/guard"
    "gitlab.com/viatorem/acgranter/pkg/hasher"
    "io"
    "io/ioutil"
    "log"
    "net"
    "net/http"
    "os"
    "strings"
)

var (
    SocketMode = 0733
)

type AccountSource struct {
    Auth   db.Auth     `yaml:"auth"`
    Header string      `yaml:"header"`
    Path   string      `yaml:"path"`
    Hash   hasher.Hash `yaml:"hash"`
    Salt   string      `yaml:"salt"`
}

type Config struct {
    Verbose              bool            `yaml:"verbose"`
    Debug                bool            `yaml:"debug"`
    SocketPath           string          `yaml:"socket_path"`
    AccountSources       []AccountSource `yaml:"account_sources"`
    Realm                string          `yaml:"realm"`
    IpHeader             string          `yaml:"ip_header"`
    SuccessHeader        string          `yaml:"success_header"`
    SuccessHeaderValue   string          `yaml:"success_header_value"`
    AccountHeader        string          `yaml:"account_header"`
    BannedHttpCode       int             `yaml:"banned_http_code"`
    UnauthorizedResponse string          `yaml:"unauthorized_response"`
    SuccessResponse      string          `yaml:"success_response"`
    BannedResponse       string          `yaml:"banned_response"`
    Guard                *guard.Config   `yaml:"guard"`
}

var (
    Verbose         bool = false
    VerbosePayload  bool = false
    inputBufferSize int  = 512
)

type Server struct {
    config     Config
    newConfig  Config
    waHeader   string
    httpServer http.Server
    input      net.Listener
    accountDbs []db.Db
    guard      *guard.Guard
}

func New(config Config) (s *Server, err error) {
    err = Validate(&config)
    if err != nil {
        err = fmt.Errorf("Validate err: %v", err)
        return
    }

    s = &Server{}

    if config.Guard != nil {
        guard.PreLoadConfig(config.Guard, config.Verbose)
    }

    s.setConfig(config)

    http.HandleFunc("/", s.HandleRequest)
    return
}

func (s *Server) HandleRequest(w http.ResponseWriter, r *http.Request) {
    var ip string
    if len(s.config.IpHeader) != 0 {
        ip = r.Header.Get(s.config.IpHeader)
    } else {
        var err error
        ip, _, err = net.SplitHostPort(strings.TrimSpace(r.RemoteAddr))
        if err != nil {
            ip = ""
        }
    }

    if s.config.Debug {
        log.Println(ip, "Request:", r.RequestURI, "Headers:", r.Header)
    }

    var toBan bool
    if s.guard != nil {
        toBan = s.guard.MakePriorResolution(ip)
        if toBan && s.config.Debug {
            log.Println(ip, "Banned by Prior Resolution")
        }
    }
    if toBan {
        http.Error(w, s.config.BannedResponse, s.config.BannedHttpCode)
        return
    }

    var account string
    var ok, found bool
    for _, accountDb := range s.accountDbs {
        account, ok, found = accountDb.GetAccount(r, ip)
        if found {
            break
        }
    }

    if !ok {
        if s.guard != nil {
            toBan = s.guard.MakeResolutionOnFail(ip, found, account)
            if toBan && s.config.Debug {
                log.Println(ip, "Banned by Resolution On Fail")
            }
        }
        if toBan {
            http.Error(w, s.config.BannedResponse, s.config.BannedHttpCode)
            return
        } else {
            if len(s.waHeader) != 0 {
                w.Header().Set("WWW-Authenticate", s.waHeader)
            }
            http.Error(w, s.config.UnauthorizedResponse, http.StatusUnauthorized)
        }
        return
    }

    if len(s.config.SuccessHeader) != 0 {
        w.Header().Set(s.config.SuccessHeader, s.config.SuccessHeaderValue)
    }

    if len(s.config.AccountHeader) != 0 {
        w.Header().Set(s.config.AccountHeader, account)
    }

    io.WriteString(w, s.config.SuccessResponse)
}

func Validate(config *Config) (err error) {
    if len(config.SocketPath) == 0 {
        err = fmt.Errorf("empty socket_path")
        return
    }
    if len(config.AccountSources) == 0 {
        err = fmt.Errorf("you have to set at least one account source")
        return
    }
    return
}

func (s *Server) Apply(newConfig Config) (err error) {
    err = Validate(&newConfig)
    if err != nil {
        err = fmt.Errorf("Validate err: %v", err)
        return
    }

    if newConfig.Guard != nil {
        err = guard.PreLoadConfig(newConfig.Guard, s.config.Verbose)
        if err != nil {
            err = fmt.Errorf("guard.PreLoadConfig err: %v", err)
            return
        }
    }

    s.newConfig = newConfig

    err = s.httpServer.Close()
    return
}

func (s *Server) setConfig(config Config) {
    s.config = config

    if s.config.BannedHttpCode == 0 {
        s.config.BannedHttpCode = http.StatusForbidden
    }

    if len(s.config.SuccessHeader) != 0 && len(s.config.SuccessHeaderValue) == 0 {
        s.config.SuccessHeaderValue = "Authorized"
    }

    if len(s.accountDbs) > len(s.config.AccountSources) {
        s.accountDbs = s.accountDbs[:len(s.config.AccountSources)]
    }

    hasBasic := false
    amount := len(s.accountDbs)
    for index, source := range s.config.AccountSources {
        if source.Auth == db.AuthBasic {
            hasBasic = true
        }

        if index < amount {
            s.accountDbs[index].SetParams(source.Auth, source.Header, source.Hash, source.Salt)
        } else {
            accountDb := db.New(source.Auth, source.Header, source.Hash, source.Salt)
            s.accountDbs = append(s.accountDbs, *accountDb)
        }
    }

    if hasBasic {
        realm := "Restricted"
        if len(s.config.Realm) != 0 {
            realm = strings.Replace(s.config.Realm, "\"", "\\\"", -1)
        }
        s.waHeader = fmt.Sprintf("Basic realm=\"%s\", charset=\"UTF-8\"", realm)
    } else {
        s.waHeader = ""
    }

    if s.config.Guard != nil {
        s.guard = guard.New(*s.config.Guard)
    } else {
        s.guard = nil
    }
}

func (s *Server) RefreshDb() (err error) {
    for index, source := range s.config.AccountSources {
        if index >= len(s.accountDbs) {
            err = fmt.Errorf("unexpected error: no db on index %d", index)
            return
        }
        var scvData []byte
        scvData, err = ioutil.ReadFile(source.Path)
        if err != nil {
            err = fmt.Errorf("ioutil.ReadFile err: %v", err)
            return
        }
        err = s.accountDbs[index].UpdateDbFromCsv(scvData, false)
        if err != nil {
            err = fmt.Errorf("UpdateDbFromCsv for index %d err: %v", index, err)
            return
        }
    }
    return
}

func (s *Server) listen() (err error) {
    info, err := os.Stat(s.config.SocketPath)
    if err != nil {
        if !os.IsNotExist(err) {
            err = fmt.Errorf("os.Stat err: %v", err)
            return
        }
    } else {
        if info.IsDir() {
            err = fmt.Errorf("socket_path is dir")
            return
        }
        err = os.Remove(s.config.SocketPath)
        if err != nil {
            err = fmt.Errorf("os.Remove err: %v", err)
            return
        }
    }

    s.input, err = net.Listen("unix", s.config.SocketPath)
    if err != nil {
        err = fmt.Errorf("net.Listen err: %v", err)
        return
    }

    err = os.Chmod(s.config.SocketPath, os.FileMode(SocketMode))
    if err != nil {
        err = fmt.Errorf("os.Chmod err: %v", err)
        return
    }

    s.httpServer = http.Server{}

    err = s.httpServer.Serve(s.input)
    if err != nil {
        if err == http.ErrServerClosed {
            err = nil
            return
        }
        err = fmt.Errorf("server.Serve err: %v", err)
        return
    }
    return
}

func (s *Server) Run() (err error) {
    err = s.RefreshDb()
    if err != nil {
        err = fmt.Errorf("server.RefreshDb err: %v", err)
        return
    }

    for {
        err = s.listen()
        if err != nil {
            return
        }

        s.setConfig(s.newConfig)

        if s.config.Verbose {
            log.Println("restaring socket listening")
        }
    }
    return
}

func (a *Server) Stop() {
    a.httpServer.Close()
}
