package hasher

import (
    "github.com/stretchr/testify/assert"
    "testing"

    "gopkg.in/yaml.v2"
)

func TestHashMarshalYAML(t *testing.T) {
    _, err := yaml.Marshal(Hash(^uint(0)))
    assert.NotNil(t, err, "Non-supported Hash must not be marshaled")

    _, err = yaml.Marshal(HashZero)
    assert.NotNil(t, err, "Zerp Hash must not be marshaled")

    label, err := yaml.Marshal(HashShake256)
    assert.Nil(t, err)
    assert.Equal(t, HashShake256Label+"\n", string(label))
}

func TestHashUnmarshalYAML(t *testing.T) {
    var x struct {
        Value Hash `yaml:"value"`
    }
    assert.NotNil(t, yaml.Unmarshal([]byte("value: \"\""), &x), "Empty string must not be unmarshaled")

    assert.NotNil(t, yaml.Unmarshal([]byte("md4"), &x.Value), "Non-Supported hash must not be unmarshaled")

    assert.Nil(t, yaml.Unmarshal([]byte(HashShake256Label), &x.Value))
    assert.Equal(t, HashShake256, x.Value)
}

func TestHasher(t *testing.T) {
    assert.Equal(t,
        "test",
        New(HashZero, "tEsT").Hash("test"),
    )

    assert.Equal(t,
        "5d95de72c6132467b813bca8c0f77848e7bc0b4500f8b9b413c9dbc9f79ae41e",
        New(HashShake256, "tEsT").Hash("test"),
    )

    assert.Equal(t,
        "5d95de72c6132467b813bca8c0f77848e7bc0b4500f8b9b413c9dbc9f79ae41e8f518afcc250d1bf00627497bbd2b8abed074dfbafc03acef2b760767817f141",
        New(HashShake256_64, "tEsT").Hash("test"),
    )

    assert.Equal(t,
        "91afb738f410fb86bb81814d3a619d31",
        New(HashMd5, "tEsT").Hash("test"),
    )

    assert.Equal(t,
        "ff16322a4ad15e2e4f9826a222bd1dfba2bb81d3",
        New(HashSha1, "tEsT").Hash("test"),
    )

    assert.Equal(t,
        "cb82433bd271dcbcf036113ece37786a312eabe16e9eda314c5652d1",
        New(HashSha224, "tEsT").Hash("test"),
    )

    assert.Equal(t,
        "991b083aa0e03de5fb5f398819e2500a045c192541b4c43dae0a621bb7df337d",
        New(HashSha256, "tEsT").Hash("test"),
    )

    assert.Equal(t,
        "5c1c0212385be3ed7830840bc334eb6ab9fe21a880022ec88c7d96e390424e40",
        New(HashSha3, "tEsT").Hash("test"),
    )

    assert.Equal(t,
        "265ef2aa827759881e8818fd1b8f0049b820136698dddd9b9b38678e0824e90e7033c70c5a9b14a87a1c583b7a661c7b594cc5442227fd6a3012234cef1d9d7d",
        New(HashSha3_512, "tEsT").Hash("test"),
    )
}
