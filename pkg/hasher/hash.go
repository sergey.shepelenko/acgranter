package hasher

import (
    "fmt"
    "strings"
)

const (
    HashZero Hash = iota
    HashShake256
    HashShake256_64
    HashMd5
    HashSha1
    HashSha224
    HashSha256
    HashSha3
    HashSha3_512
)

const (
    HashZeroLabel        = ""
    HashShake256Label    = "shake256"
    HashShake256_64Label = "shake256_64"
    HashMd5Label         = "md5"
    HashSha1Label        = "sha1"
    HashSha224Label      = "sha224"
    HashSha256Label      = "sha256"
    HashSha3Label        = "sha3"
    HashSha3_512Label    = "sha3_512"
)

type Hash uint

func (a Hash) String() string {
    switch a {
    case HashShake256:
        return HashShake256Label
    case HashShake256_64:
        return HashShake256_64Label
    case HashMd5:
        return HashMd5Label
    case HashSha1:
        return HashSha1Label
    case HashSha224:
        return HashSha224Label
    case HashSha256:
        return HashSha256Label
    case HashSha3:
        return HashSha3Label
    case HashSha3_512:
        return HashSha3_512Label
    }
    return ""
}

func (a Hash) IsZero() bool {
    return a == HashZero
}

func HashFromString(label string) (h Hash, err error) {
    label = strings.ToLower(label)
    switch label {
    case HashShake256Label:
        h = HashShake256
    case HashShake256_64Label:
        h = HashShake256_64
    case HashMd5Label:
        h = HashMd5
    case HashSha1Label:
        h = HashSha1
    case HashSha224Label:
        h = HashSha224
    case HashSha256Label:
        h = HashSha256
    case HashSha3Label:
        h = HashSha3
    case HashSha3_512Label:
        h = HashSha3_512
    default:
        err = fmt.Errorf("Hash '%s' is not supported", label)
    }
    return
}

func (h *Hash) UnmarshalYAML(unmarshal func(interface{}) error) (err error) {
    var s string
    err = unmarshal(&s)
    if err != nil {
        return
    }
    *h, err = HashFromString(s)
    return
}

func (h Hash) MarshalYAML() (value interface{}, err error) {
    if h == HashZero {
        err = fmt.Errorf("Special Zero Hash must not be marshaled")
        return
    }
    s := h.String()
    if len(s) == 0 {
        err = fmt.Errorf("Hash must not have empty label")
        return
    }
    value = s
    return
}
