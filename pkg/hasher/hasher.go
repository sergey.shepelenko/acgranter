package hasher

import (
    "crypto/md5"
    "crypto/sha1"
    "crypto/sha256"
    "encoding/hex"
    "golang.org/x/crypto/sha3"
    commonHash "hash"
    "log"
)

var (
    Verbose     = true
    DefaultSalt = "NaCl"
)

type Hasher struct {
    salt []byte
    Hash func(string) string
}

func New(hash Hash, salt string) (h *Hasher) {
    h = &Hasher{
        salt: []byte(salt),
    }
    switch hash {
    case HashShake256_64:
        h.Hash = h.shake256_64
    case HashMd5:
        h.Hash = h.md5
    case HashSha1:
        h.Hash = h.sha1
    case HashSha224:
        h.Hash = h.sha224
    case HashSha256:
        h.Hash = h.sha256
    case HashShake256:
        h.Hash = h.shake256
    case HashSha3:
        h.Hash = h.sha3
    case HashSha3_512:
        h.Hash = h.sha3_512
    default:
        h.Hash = h.plain
        if Verbose {
            log.Println("Warning! Hasher do not use any hash algorithm!")
        }
    }
    return
}

func SupportedHashes() string {
    return `Supported hashes:
    shake256    : Shake256 with 32 bytes of output
    shake256_64 : Shake256 with 64 bytes of output
    md5         : MD5
    sha1        : SHA1
    sha224      : SHA2‑224
    sha256      : SHA2‑256
    sha3        : SHA3‑256
    sha3_512    : SHA3‑512`
}

func (h Hasher) getSalt() []byte {
    return append([]byte{}, h.salt...)
}

func (h Hasher) getStandardHashSum(a commonHash.Hash, input string) string {
    a.Write([]byte(input))
    a.Write(h.getSalt())
    return hex.EncodeToString(a.Sum(nil))
}

func (h Hasher) plain(input string) string {
    return input
}

func (h Hasher) shake256(input string) string {
    a := sha3.NewCShake256([]byte("hShk256"), h.getSalt())
    output := make([]byte, 32)
    a.Write([]byte(input))
    a.Read(output)
    return hex.EncodeToString(output)
}

func (h Hasher) shake256_64(input string) string {
    a := sha3.NewCShake256([]byte("hShk256"), h.getSalt())
    output := make([]byte, 64)
    a.Write([]byte(input))
    a.Read(output)
    return hex.EncodeToString(output)
}

func (h Hasher) md5(input string) string {
    return h.getStandardHashSum(md5.New(), input)
}

func (h Hasher) sha1(input string) string {
    return h.getStandardHashSum(sha1.New(), input)
}

func (h Hasher) sha224(input string) string {
    return h.getStandardHashSum(sha256.New224(), input)
}

func (h Hasher) sha256(input string) string {
    return h.getStandardHashSum(sha256.New(), input)
}

func (h Hasher) sha3(input string) string {
    return h.getStandardHashSum(sha3.New256(), input)
}

func (h Hasher) sha3_512(input string) string {
    return h.getStandardHashSum(sha3.New512(), input)
}
