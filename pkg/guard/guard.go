package guard

import (
    "bufio"
    "fmt"
    "gitlab.com/viatorem/acgranter/pkg/binaryList"
    "gitlab.com/viatorem/acgranter/pkg/counter"
    "log"
    "net"
    "os"
    "os/exec"
    "strings"
    "time"
)

type Config struct {
    Verbose         bool     `yaml:"verbose"`
    CountPeriod     int64    `yaml:"count_period"`
    BanPeriod       int64    `yaml:"ban_period"`
    BanAmount       int64    `yaml:"ban_amount"`
    BlacklistAmount int64    `yaml:"blacklist_amount"`
    NotifyAmount    int64    `yaml:"notify_amount"`
    NotifyBanAmount int64    `yaml:"notify_ban_amount"`
    NotifyCmd       string   `yaml:"notify_cmd"`
    NotifyBanCmd    string   `yaml:"notify_ban_cmd"`
    Whitelist       []string `yaml:"whitelist"`
    Blacklist       []string `yaml:"blacklist"`
    WhitelistPath   string   `yaml:"whitelist_path"`
    BlacklistPath   string   `yaml:"blacklist_path"`
}

type Guard struct {
    config         Config
    failed         *counter.Counter
    banned         *counter.Counter
    whiteBlackList *binaryList.BinaryList
}

func New(config Config) (g *Guard) {
    g = &Guard{
        config: config,
        failed: counter.New(config.CountPeriod),
        banned: counter.New(config.BanPeriod),
    }

    if len(config.Whitelist) != 0 || len(config.Blacklist) != 0 {
        g.whiteBlackList = binaryList.New()
        g.whiteBlackList.Set(config.Whitelist, config.Blacklist)
    }

    return
}

func loadIpsFromFile(path string, verbose bool) (ips []string, err error) {
    file, err := os.Open(path)
    if err != nil {
        err = fmt.Errorf("os.Open err: %v", err)
        return
    }
    defer file.Close()

    scanner := bufio.NewScanner(file)
    for scanner.Scan() {
        ip := scanner.Text()
        ip = strings.TrimSpace(ip)
        if len(ip) == 0 {
            continue
        }
        if net.ParseIP(ip) != nil {
            ips = append(ips, ip)
        } else if verbose {
            log.Println(fmt.Sprintf("incorrect ip: '%s'", ip))
        }
    }
    err = scanner.Err()
    return

}

func PreLoadConfig(config *Config, verbose bool) (err error) {
    var ips []string
    if len(config.WhitelistPath) != 0 {
        ips, err = loadIpsFromFile(config.WhitelistPath, verbose)
        if err != nil {
            err = fmt.Errorf("error loading Whitelist IPs from %s", config.WhitelistPath)
            return
        }
        config.Whitelist = append(config.Whitelist, ips...)
    }
    if len(config.BlacklistPath) != 0 {
        ips, err = loadIpsFromFile(config.BlacklistPath, verbose)
        if err != nil {
            err = fmt.Errorf("error loading Blacklist IPs from %s", config.BlacklistPath)
            return
        }
        config.Blacklist = append(config.Blacklist, ips...)
    }
    return
}

func (g Guard) MakePriorResolution(ip string) (toBan bool) {
    if g.whiteBlackList != nil {
        whiteBlackValue, found := g.whiteBlackList.Get(ip)
        if found {
            toBan = !whiteBlackValue
            return
        }
    }
    amount, prevHit := g.banned.Hit(ip, false)
    toBan = (amount != 0)
    if g.config.BlacklistAmount != 0 && amount > g.config.BlacklistAmount {
        g.whiteBlackList.AddNegative(ip)
        g.banned.Delete(ip)
    }
    if g.config.NotifyBanAmount != 0 && amount > g.config.NotifyBanAmount {
        args := []string{
            ip,
            fmt.Sprintf("%d", amount),
            prevHit.In(time.UTC).Format(time.RFC3339),
            time.Now().In(time.UTC).Format(time.RFC3339),
        }
        g.exec(g.config.NotifyBanCmd, args...)
    }
    return
}

func (g Guard) MakeResolutionOnFail(ip string, found bool, account string) (toBan bool) {
    if g.whiteBlackList != nil {
        whiteBlackValue, found := g.whiteBlackList.Get(ip)
        if found {
            toBan = !whiteBlackValue
            return
        }
    }

    amount, prevHit := g.failed.Hit(ip, true)
    if g.config.BanAmount != 0 && amount > g.config.BanAmount {
        g.failed.Delete(ip)
        g.banned.Hit(ip, true)
    }

    if g.config.NotifyAmount != 0 && amount > g.config.NotifyAmount {
        foundValue := "nomatch"
        if found {
            foundValue = "match"
        }
        args := []string{
            ip,
            fmt.Sprintf("%d", amount),
            prevHit.In(time.UTC).Format(time.RFC3339),
            time.Now().In(time.UTC).Format(time.RFC3339),
            foundValue,
            account,
        }
        g.exec(g.config.NotifyBanCmd, args...)
    }
    return
}

func (g Guard) exec(cmd string, args ...string) {
    if len(cmd) == 0 {
        return
    }
    go func(cmd string, args ...string) {
        out, err := exec.Command(cmd, args...).CombinedOutput()
        if err != nil {
            log.Println("Notify cmd err:", err, out)
        }
        if g.config.Verbose {
            log.Println("Notify cmd executed with output:", string(out))
        }
    }(cmd, args...)
    return
}
