# Acgranter

Service to authorize requests and to defend against brute force attacks.

Service should be run as systemd unit. All https requests listened via unix socket.

## Sample configuration for Nginx with PHP-FPM

```
server {
        location = /auth {
            internal;
            proxy_pass              http://unix:/var/run/acgranter.sock;
            proxy_pass_request_body off;
            proxy_set_header        Content-Length "";
            proxy_set_header        X-Original-URI $request_uri;
            proxy_set_header        Host $host;
            proxy_set_header        X-Real-IP $remote_addr;
            proxy_set_header        X-Forwarded-Proto https;
        }

        location / {
            auth_request            /auth;
            auth_request_set        $auth_status $upstream_status;
            auth_request_set        $acgranter_account $upstream_http_acgranter_account;
            auth_request_set        $acgranter_status $upstream_http_acgranter_status;

            try_files $uri /index.php$is_args$args;
        }

        location ~ \.php$ {
                include snippets/fastcgi-php.conf;
                fastcgi_pass        unix:/run/php/php7.4-fpm.sock;
                fastcgi_param       SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
                fastcgi_param       DOCUMENT_ROOT $realpath_root;

                fastcgi_param       REMOTE_USER $acgranter_account;
                fastcgi_param       ACGRANTER_STATUS $acgranter_status;

                internal;
        }
}
```

This configuration will transmit to PHP-FPM only successfully authorized requests with $_SERVER parameters:
 - `$_SERVER['REMOTE_USER']` - Account name provided by authorization process
 - `$_SERVER['ACGRANTER_STATUS']` - Configuration based Parameters for additional confirmation that authorization was successful and account name obtained from *acgranter* service.

## Sample configuration for Acgranter

```yaml
#socket to listen for requests to authorize
socket_path: /var/run/acgranter.sock

#debug mode, very verbose
debug: true

#print some non-critical warnings
verbose: true

#response for successfully authorized request
response: ""

#Basic Auth realm
realm: Restricted

#header to read IP by acgranter service
ip_header: X-Real-Ip

#header that acgranter service should set in case of successful authorization.
success_header: acgranter_status

#value for success_header. Default: Authorized
success_header_value: authorized

#header with value of account name that acgranter service should set in case of successful authorization.
account_header: acgranter_account

#HTTP response status codes to use for banned requests
banned_http_code: 403

#HTTP response for ordinary unsuccessful requests
unauthorized_response: "Unauthorized"

#HTTP response for successful requests
success_response: ""

#HTTP response for banned requests
banned_response: "Banned"

#description of CSV files with credentials of accounts
# auth - type of authorization.
#   Supported: basic, token, ip
# path - path to CSV
# hash - algorithm to hash password, token or ip.
#   Supported: shake256, shake256_64, md5, sha1, sha224, sha256, sha3, sha3_512
#   For more information use acgranter-hash command.
# salt - salt string for hash
# header - header to reade token for token based authorization
#All sources checked consecutively and used first successful one
#CSV files should have two columns: the first is for account, the second is for password, token or ip.
#In case of using hash the second column should contain hashed values.
account_sources:
# standard HTTP basic authorization
  - auth: basic
    path: /path/to/users_and_passwords.csv
    hash: shake256
    salt: aCgRaNtEr
# standard HTTP token based authorization
  - auth: token
    header: Api-Token
    path: /path/to/users_and_tokens.csv
# authorization based on mapping accounts by ip
  - auth: ip
    path: /path/to/users_and_ips.csv

#whether to count unsuccessful request to ban by IP
guard:
# print some non-critical warnings
  verbose: true

# list of IPs to not ban in any case
  whitelist:
    - 127.0.0.1

# list of IPs to always ban
  blacklist:
    - 1.1.1.1

# path to file with whitelist IPS, one IP on each row
  whitelist_path: /path/to/white_ips

# path to file with whitelist IPS, one IP on each row
  blacklist_path: /path/to/black_ips

# maximal interval in seconds between unsuccessful requests to continue increment counter
  count_period: 600

# interval in seconds to ban
  ban_period: 3600

# threshold of unsuccessful requests to ban
  ban_amount: 1000

# threshold of banned requests to add to blacklist
  blacklist_amount: 10000

# threshold of unsuccessful requests to execute command for notifying about abusing from IP
  notify_amount: 3000

# threshold of banned requests to execute command for notifying about abusing from banned IP
  notify_ban_amount: 3000

# path command for notifying about abusing from IP
  notify_cmd: /path/to/cmd

# path command for notifying about abusing from banned IP
  notify_ban_cmd: /path/to/cmd

#kill nohup signal case rotating logs and reloading config file and CSVs if they changed.
#notify_ban_cmd can be used for adding IP to blacklist file and reload the service to achieve a permanent ban.
```