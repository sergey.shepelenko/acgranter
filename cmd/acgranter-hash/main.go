package main

import (
    "flag"
    "fmt"
    "gitlab.com/viatorem/acgranter/pkg/hasher"
    "io/ioutil"
    "log"
    "os"
)

var (
    hash = flag.String("hash", hasher.HashShake256.String(), "Hash")
    salt = flag.String("salt", hasher.DefaultSalt, "Salt")
)

func printUsage() {
    fmt.Fprintf(flag.CommandLine.Output(), "Usage of %s:\n", os.Args[0])
    fmt.Fprintf(flag.CommandLine.Output(), "%s [OPTIONS] INPUT\n", os.Args[0])
    fmt.Fprintf(flag.CommandLine.Output(), "When no INPUT is passed a standard input is used.\n")
    flag.PrintDefaults()
    fmt.Fprintf(flag.CommandLine.Output(), "%s\n", hasher.SupportedHashes())

}

func printError(format string, arg ...interface{}) {
    fmt.Fprintln(flag.CommandLine.Output(), fmt.Sprintf(format, arg...))
}

func main() {
    flag.Usage = printUsage
    flag.Parse()

    var input string

    stdinStat, err := os.Stdin.Stat()
    if err != nil {
        log.Panicln("os.Stdin.Stat err:", err)
    }
    if (stdinStat.Mode() & os.ModeCharDevice) == 0 {
        data, err := ioutil.ReadAll(os.Stdin)
        if err != nil {
            printError("stdin ioutil.ReadAll err: %v", err)
            return
        }
        input = string(data)
    } else {
        args := flag.Args()
        if len(args) == 0 {
            flag.Usage()
            return
        }
        input = flag.Arg(0)
    }

    hasherHash, err := hasher.HashFromString(*hash)
    if err != nil {
        log.Panicln("hasher.HashFromString err:", err)
    }

    if hasherHash.IsZero() {
        log.Panicln("No hash algorithm found")
    }

    h := hasher.New(hasherHash, *salt)

    fmt.Println(h.Hash(input))
}
