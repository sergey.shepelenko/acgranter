package main

import (
    "bytes"
    "crypto/md5"
    "flag"
    "fmt"
    "gitlab.com/viatorem/acgranter/pkg/server"
    "gopkg.in/yaml.v2"
    "io/ioutil"
    "log"
    "os"
    "os/signal"
    "runtime"
    "syscall"
)

var (
    configPath = flag.String("config", "../../configs/config.yml", "Path to config")
    logPath    = flag.String("log", "", "Path to log file")
    configHash [16]byte
)

func main() {
    runtime.GOMAXPROCS(runtime.NumCPU())
    flag.Parse()
    log.SetFlags(log.LstdFlags | log.Lshortfile)

    err := setLogging()
    if err != nil {
        log.Fatalln("setLogging err:", err)
        return
    }

    config, err := readConfig()
    if err != nil {
        log.Fatalln("readConfig err:", err)
        return
    }
    if config == nil {
        log.Fatalln("empty config file")
        return
    }

    app, err := server.New(*config)
    if err != nil {
        log.Fatalln("app.New err:", err)
    }

    log.Println("acgranter started.")

    signalChan := make(chan os.Signal, 1)
    signal.Notify(signalChan, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)

    exitChan := make(chan int)
    go func() {
        for {
            s := <-signalChan
            switch s {
            // kill -SIGINT  or Ctrl+c
            case syscall.SIGINT:
                log.Println("Interaption signal, doing nothing, exiting.")
                exitChan <- 0

            case syscall.SIGHUP:
                newConfig, err := readConfig()
                if err != nil {
                    log.Println("readConfig err:", err)
                    continue
                }
                if newConfig != nil {
                    log.Println("reloading config")
                    err = app.Apply(*newConfig)
                    if err != nil {
                        log.Println("app.Apply err:", err)
                        continue
                    }
                }

                err = app.RefreshDb()
                if err != nil {
                    log.Println("app.RefreshDb err:", err)
                    continue
                }

                err = setLogging()
                if err != nil {
                    log.Println("setLogging err:", err)
                    continue
                }

            case syscall.SIGTERM, syscall.SIGQUIT:
                log.Println("Caught ", s.String())
                log.Println("Closing socket...")
                app.Stop()
                log.Println("Exiting.")
                exitChan <- 0

            default:
                log.Println("Unknown signal. Exiting.")
                exitChan <- 1
            }
        }
    }()

    go func(app *server.Server) {
        err := app.Run()
        if err != nil {
            log.Println("app.Run err: ", err)
            exitChan <- 1
        }
    }(app)

    code := <-exitChan
    os.Exit(code)
}

func readConfig() (config *server.Config, err error) {
    data, err := ioutil.ReadFile(*configPath)
    if err != nil {
        err = fmt.Errorf("ioutil.ReadFile err: %v", err)
        return
    }

    newConfigHash := md5.Sum(data)
    if bytes.Equal(newConfigHash[:], configHash[:]) {
        return
    }

    config = new(server.Config)
    err = yaml.Unmarshal(data, &config)
    if err != nil {
        err = fmt.Errorf("yaml.Unmarshal err: %v", err)
        return
    }
    configHash = newConfigHash
    return
}

var currentLog *os.File

func setLogging() (err error) {
    if currentLog != nil {
        currentLog.Close()
    }
    if len(*logPath) == 0 {
        if currentLog == nil {
            return
        }
        os.Stderr = os.NewFile(uintptr(syscall.Stderr), "/dev/stderr")
        log.SetOutput(os.Stderr)
        return
    }

    currentLog, err := os.OpenFile(*logPath, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
    if err != nil {
        err = fmt.Errorf("os.OpenFile err: %v", err)
        return err
    }
    log.SetOutput(currentLog)
    return
}
